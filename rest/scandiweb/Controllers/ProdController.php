<?php
    namespace Scandiweb\Controllers;
    
    class ProdController
    {
        function prodList()
        {
            global $database;
            $rows = $database->select("products",array('*'),'',"ORDER BY id ASC");
            $list = array();
            
            foreach ($rows["data"] as $row) {
                
                //create the product object
                $product = $this->createClass($row);
                
                //set the product's attributes that needs formatting for output
                $product->type = $product->getType();
                $product->price = $product->getPrice();
                
                //add the product to the list
                array_push($list,$product);
            }
            $database->resp(200, $list);
        }
        
        
        function prodAdd()
        {
            global $database;
            $post = json_decode($this->var2);
            
            //check if there is a product with the same sku
            $rows = $database->select("products",array('sku'),array('sku' => $post->sku),"");
            if($rows['status'] != 'success'){
                //create the product object
                $product = $this->createClass($post);
            
                //save the product into the database
                $database->insert("products",$product);
                $database->resp(200, $product);
            }
            else{
                $product = false;
                $database->resp(406, $product);
            }
        }
        
        function prodDel()
        {
            global $database;
            
            //get the product ids that needs to be deleted
            $ids = explode(",",$this->var2);
            foreach($ids as $id){
                if($id != false){
                    $database->$conn->query("DELETE FROM products WHERE id = '".$id."'");
                }
            }
            $database->resp(200, $ids);
        }
        
        function createClass($object)
        {
            //get the class name based on the product's type
            $class = 'Scandiweb\Objects\_'.$object->type."Object";
            $class = str_replace("_","",$class);
                
            //get the product type's attributes
            $reflection = new \ReflectionClass("\\".$class);
            $attributes = $reflection->getProperties(\ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED);
                
            //set the product type's attributes value
            $construct = array();
            foreach ($attributes AS $attr) {
                $value = $attr->name;
                $construct[$attr->name] = $object->$value;
            }
                
            //create the product based on the type
            return $product = new $class($construct);
        }
    }