<?php
    namespace Scandiweb\Config;
    use \PDO;
    
    define('DB_USERNAME', 'scandiweb');
    define('DB_PASSWORD', 'qL2zH4fN8bpY5r11');
    define('DB_HOST', '127.0.0.1');
    define('DB_NAME', 'scandiweb');
    
    class Database
    {
        public $conn;
        private $err;
        function __construct()
        {
            $dsn = 'mysql:host='.DB_HOST.';port=3311;dbname='.DB_NAME.';charset=utf8';
            try {
                $this->$conn = new PDO($dsn, DB_USERNAME, DB_PASSWORD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
            } catch (PDOException $e) {
                echo $e->getMessage();
                $response["status"] = "error";
                $response["message"] = 'Connection failed: ' . $e->getMessage();
                $response["data"] = null;
                exit;
            }
        }
        
        function resp($status_code, $response)
        {
            http_response_code($status_code);
            echo json_encode($response);
        }
        
        function select($table, $columns, $where, $order)
        {
            try{
                $a = array();
                $w = "";
                $cols = "";
                $where_string = "";
                if(is_array($where)){
                    foreach ($where as $key => $value) {
                        $w .= " and " .$key. " like :".$key;
                        $a[":".$key] = $value;
                    }
                    $where_string = "where 1=1 ". $w;
                }
                for($i = 0;$i<count($columns);$i++){
                    $cols.= $columns[$i].",";
                }
                $cols = substr($cols, 0, -1);
                $stmt = $this->$conn->prepare("select ".$cols." from ".$table." ".$where_string." ".$order);
                $stmt->execute($a);
                $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
                if(count($rows)<=0){
                    $response["status"] = "warning";
                    $response["message"] = "No data found.";
                }else{
                    $response["status"] = "success";
                    $response["message"] = "Data selected from database";
                }
                    $response["data"] = $rows;
            }catch(PDOException $e){
                $response["status"] = "error";
                $response["message"] = 'Select Failed: ' .$e->getMessage();
                $response["data"] = null;
            }
            $response['lekerdezes'] = "select ".$cols." from ".$table." where 1=1 ". $w." ".$order;
            return $response; 
        }
        
        function insert($table, $data)
        {
            $q = $this->$conn->prepare("DESCRIBE ".$table);
            $q->execute();
            $getFields  = $q->fetchAll(PDO::FETCH_COLUMN);
            array_shift($getFields);
            $dbFieldCount = count( $getFields );
            $implodedFields = implode(", :", $getFields);
            $sql = "INSERT INTO ".$table."(".implode(", ", $getFields).") VALUES (:".$implodedFields.")";
            $insert = $this->$conn->prepare( $sql );
            $none        = "";
            foreach ( $getFields as $dbKey => $dbValue ) {
                if(isset($data->$dbValue)) {
                    $insert->bindValue(":$dbValue", $data->$dbValue);
                }
                else {
                    $insert->bindValue(":$dbValue", $none);
                }
            }
            $insert->execute();
            $last_insert_id    = $this->$conn->lastInsertId();
            return $last_insert_id; 
        } 
    }