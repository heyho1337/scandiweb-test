<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: access");
    header("Access-Control-Allow-Methods: GET,POST");
    header("Access-Control-Allow-Credentials: true");
    header('Content-Type: application/json');
    
    //autoload the classes
    include_once '../../../vendor/autoload.php';
    
    use Scandiweb\Controllers\ProdController as prodController;
    $prodController = new prodController();
    
    use Scandiweb\Config\Database as database;
    $database = new database();
    
    //get the posted data and call the corresponding function
    $vars = array();
    foreach($_POST as $key => $value){
        array_push($vars,$value);
        $prodController->$key = $value;
    }
    $g = $vars[0];
    $p = $prodController->$g();