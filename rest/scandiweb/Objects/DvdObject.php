<?php
    namespace Scandiweb\Objects;

    class DvdObject extends ProdObject
    {
        public $sku;
        public $name;
        public $type;
        public $price;
        public $id;
        public $size;
        
        public function __construct($construct)
        {
            foreach($construct as $key=>$value) {
                $this->$key = $value; 
            }
        }
        
        public function getSku()
        {
            return $this->sku; 
        }
        
        public function getId()
        {
            return $this->id; 
        }
        
        public function getType()
        {
            return "Size: ".$this->size." MB"; 
        }
        
        public function setType()
        {
            
        }
        
        public function getPrice()
        {
            return number_format($this->price, 2, '.', ','); 
        }
        
        public function setPrice()
        {
            return $this->price; 
        }
        
        public function getName()
        {
            return $this->name; 
        }
    }