<?php
    namespace Scandiweb\Objects;

    class FurnitureObject extends ProdObject
    { 
        public $sku;
        public $name;
        public $type;
        public $price;
        public $id;
        public $width;
        public $height;
        public $lenght;

        public function __construct($construct)
        {
            foreach($construct as $key=>$value) {
                $this->$key = $value;
            }
        }
        
        public function getSku()
        {
            return $this->sku; 
        }
        
        public function getId()
        {
            return $this->id; 
        }
        
        public function getType()
        {
            return "Dimension:".$this->height."x".$this->width."x".$this->lenght; 
        }
        
        public function setType()
        {
            
        }
        
        public function getPrice()
        {
            return number_format($this->price, 2, '.', ','); 
        }
        
        public function setPrice()
        {
            return $this->price; 
        }
        
        public function getName()
        {
            return $this->name; 
        }
    }