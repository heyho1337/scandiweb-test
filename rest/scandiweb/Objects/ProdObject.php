<?php
    namespace Scandiweb\Objects;
    
    abstract class ProdObject
    {
        abstract protected function getSku();
        
        abstract protected function getId();
        
        abstract protected function getType();
        
        abstract protected function setType();
        
        abstract protected function getPrice();
        
        abstract protected function setPrice();
        
        abstract protected function getName();
    }