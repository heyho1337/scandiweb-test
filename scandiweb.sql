-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Gép: 0.0.0.0:3311
-- Létrehozás ideje: 2022. Ápr 19. 20:24
-- Kiszolgáló verziója: 10.3.31-MariaDB-1:10.3.31+maria~focal
-- PHP verzió: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `scandiweb`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `sku` varchar(20) NOT NULL,
  `size` varchar(6) DEFAULT NULL,
  `height` varchar(4) DEFAULT NULL,
  `width` varchar(4) DEFAULT NULL,
  `lenght` varchar(4) DEFAULT NULL,
  `weight` varchar(4) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8mb4;

--
-- A tábla adatainak kiíratása `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `type`, `sku`, `size`, `height`, `width`, `lenght`, `weight`) VALUES
(11, 'Acme disc', 10000, 'Furniture', 'tr1205551', '0', '11', '12', '13', '0'),
(14, 'Acme disc', 10000, 'Dvd', 'tr1205552', '700', '0', '0', '0', '0'),
(15, 'Acme disc', 10000, 'Book', 'tr1205553', '0', '0', '0', '0', '2'),
(37, 'dvdtest2', 1554, 'Dvd', 'sku1434tr6', '700', '0', '0', '0', '0'),
(38, 'namexsss', 12, 'Furniture', 'sku7', '', '1', '2', '3', ''),
(39, 'namenewdvd', 1200, 'Dvd', 'skunewdvd8', '2048', '', '', '', ''),
(40, 'name new book', 500, 'Book', 'skunewbook9', '', '', '', '', '20'),
(42, 'NameTest000', 25, 'Dvd', 'SKUTest00010', '200', '', '', '', ''),
(43, 'NameTest001', 25, 'Book', 'SKUTest00111', '', '', '', '', '200'),
(44, 'NameTest002', 25, 'Furniture', 'SKUTest00212', '', '200', '200', '200', ''),
(45, 'NameTest000', 25, 'Dvd', 'SKUTest00013', '200', '', '', '', ''),
(46, 'NameTest001', 25, 'Book', 'SKUTest00114', '', '', '', '', '200'),
(47, '242342', 12, 'Furniture', 'awdawd15', '', '234', '234', '234', ''),
(48, 'NameTest000', 25, 'Dvd', '32454wsefefff', '200', '', '', '', ''),
(49, 'NameTest001', 25, 'Book', 'SKUTest00134', '', '', '', '', '200'),
(50, 'NameTest001', 25, 'Book', 'SKUTest00135', '', '', '', '', '200'),
(51, 'NameTest000', 25, 'Dvd', 'SKUTest00035', '200', '', '', '', ''),
(52, 'NameTest001', 25, 'Book', 'SKUTest00137', '', '', '', '', '200'),
(53, 'NameTest000', 25, 'Dvd', 'SKUTest00038', '200', '', '', '', ''),
(54, 'NameTest001', 25, 'Book', 'SKUTest00139', '', '', '', '', '200'),
(55, 'NameTest002', 25, 'Furniture', 'SKUTest00240', '', '200', '200', '200', ''),
(85, 'w3333', 10, 'Dvd', 'sku4444444444444', '10', '', '', '', ''),
(87, 'weeeeee', 10, 'Dvd', 'sku44', '10', '', '', '', ''),
(88, 'NameTest000', 25, 'Dvd', 'SKUTest000', '200', '', '', '', ''),
(89, 'NameTest001', 25, 'Book', 'SKUTest001', '', '', '', '', '200'),
(90, 'NameTest002', 25, 'Furniture', 'SKUTest002', '', '200', '200', '200', ''),
(91, 'weeeer', 123, 'Book', 'skuuurerrree', '', '', '', '', '112');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sku` (`sku`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=92;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
