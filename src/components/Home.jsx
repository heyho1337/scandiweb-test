import React, { useState, useEffect }  from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useNavigate, Link } from "react-router-dom";
import '../styles/home.scss';
export default function Home() {
	const [delProd, setDelProd] = useState([])
	const [productSchema, setProductSchema] = useState([])
	const [delId, setDelId] = useState([])
	const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [products, setProducts] = useState([]);
	const navigate = useNavigate();
	function checkChange(e) {
		//called when checbox is checked
		let elements = [];
		let ids = [];

		//get allready checked elments
		for(let i = 0; i < delProd.length; i++) {
			elements.push(delProd[i]);
			ids.push(delProd[i].value);
		}
        if(e.target.checked){
			//add checkbox for html remove
			elements.push(e.target);

			//add checkbox value for rest api
			ids.push(e.target.value);
        }else{
			//if checkbox is unchecked get the index of it and remove from  the arrays
			let elementIndex = elements.indexOf(e.target);
        	elements.splice(elementIndex, 1);
			ids.splice(elementIndex, 1);
        };
		setDelProd(elements);
		setDelId(ids);
    }

	function add(){
		navigate('/add-product');
	}
	
	function mass_del(){
		//called when mass delete button is clicked
		if(delId.length > 0){
			const requestOptions = {
				method: 'POST',
				headers: new Headers({
					'Content-Type': 'application/x-www-form-urlencoded',
				}),
				body: "var1=proddel&var2="+delId
			};
			fetch("http://cms-angular.keszul.hu/rest/scandiweb/modules/prod",requestOptions)
			.then(res => res.json())
			.then((data) => {},
			(error) => {
				setIsLoaded(true);
				setError(error);
			})
			if (error) {
				//error during load
			} else if (!isLoaded) {
				//loader
			} else {
				//after successfull api call delete the checked product's html elements
				for(let i = 0; i < delProd.length; i++) {
					delProd[i].parentNode.parentNode.parentNode.parentNode.removeChild(delProd[i].parentNode.parentNode.parentNode);
				}
				setDelProd();
			}
		}
		else{
			alert("no product was selected");
		}
	}
    useEffect(() => {
		//load the products from the rest api
		const requestOptions = {
			method: 'POST',
			headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded',
			}),
			body: "var1=prodlist"
		};
        fetch("http://cms-angular.keszul.hu/rest/scandiweb/modules/prod",requestOptions)
		.then(res => res.json())
        .then((data) => {
            setIsLoaded(true);
            setProducts(data);
			let productSchemaObj = {
				"@context":"https://schema.org",
				"@type":"ItemList",
				"name": "Products",
				"url": "http://cms-angular.keszul.hu/",
				"image": "http://cms-angular.keszul.hu/assets/images/main/og.jpg",
				"description": "Mihály Zsebi Test Task for Scandiweb productlist",
				"numberOfItems": data.length
			};
			let itemListElement = [];
			for(let i = 0; i < data.length;i++){
				let price = data[i].price.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
				let prodItem = {
					"@type":"ListItem",
					"position":i,
					"item": {
						"@type": "Product",
						"name": data[i].name,
						"description": data[i].type+" "+data[i].name+" "+data[i].sku,
						"sku": data[i].sku,
						"mpn": data[i].id,
						"url": "http://cms-angular.keszul.hu/",
						"offers": {
						"@type": "AggregateOffer",
						"highPrice": price,
						"lowPrice": price,
						"priceCurrency": "EUR"
						}
					}
				}
				itemListElement.push(prodItem);
			}
			productSchemaObj["itemListElement"] = itemListElement;
			setProductSchema(productSchemaObj);
        },
        (error) => {
            setIsLoaded(true);
            setError(error);
        })
    }, [])
    return (<>
		<script type="application/ld+json">
			{JSON.stringify(productSchema)}
		</script>
		<header>
			<h1>Product List</h1>
			<div className="right">
				<Link id="ADD" className="ADD" title="ADD" to="/add-product">ADD</Link>
				<Link id="delete-product-btn" onClick={mass_del} title="MASS DELETE" to="#">MASS DELETE</Link>
			</div>
		</header>
        <div className="content">
            <div className="prodList">
			   	{products.map(prod => (
					<div key={prod.id} className="prodWrapper">
						<div className="checkWrapper">
							<input type="checkbox" name="del_prod" value={prod.id} onChange={checkChange} className="delete-checkbox"/>
							<FontAwesomeIcon icon="check-square" />
						</div>
						<div className="prodCont">
							<span className="sku">{prod.sku}</span>
							<h2 className="name">{prod.name}</h2>
							<span className="price">{prod.price} $</span>
							<span className="type">{prod.type}</span>
						</div>
					</div>
				))
				}
			</div>
        </div>
		</>
    );
}