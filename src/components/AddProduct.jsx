import React, { useState }  from 'react';
import { Link, useNavigate } from "react-router-dom";
import '../styles/addProduct.scss';
import { useForm } from "react-hook-form";

export default function AddProduct() {
	const { register, watch, handleSubmit, formState: { errors } } = useForm();
	const watchProductType = watch("type", false);
	const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
	const navigate = useNavigate();
	
	const save = (d) => {
		//called when save button clicked for addig a product

		//send request to rest api, saving product into the database
		const requestOptions = {
			method: 'POST',
			headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded',
			}),
			body: "var1=prodadd&var2="+JSON.stringify(d)
		};
		fetch("http://cms-angular.keszul.hu/rest/scandiweb/modules/prod",requestOptions)
		.then(res => res.json())
		.then((data) => {
			if(data == false){
				alert("This SKU is allready in use by another product");
			}
			else{
				navigate('/');
			}
		},
		(error) => {
			setIsLoaded(true);
			setError(error);
		})
		if (error) {
			//error during load
		} else if (!isLoaded) {
			//loader
		} else {
			
		}
	}

	function cancel(){
		navigate('/')
	}

	function add(){
		navigate('/add-product');
	}

	return (<>
        <header>
			<h1>Product Add</h1>
			<div className="right">
				<Link onClick={handleSubmit(save)} id="save-product-btn" title="Save" to="#">Save</Link>
				<button onClick={cancel}>CANCEL</button>
			</div>
		</header>
		<div className="content">
			<form id="product_form">
				<div className="left">
					<div className="form-control">
						<label htmlFor="sku">SKU</label>
						{errors.sku && <span className="error_label">This field is required</span>}
						<input required id="sku" type="text" {...register('sku', { required: true })} placeholder="unique identifier for every product"/>
					</div>
					<div className="form-control">
						<label htmlFor="name">Name</label>
						{errors.name && <span className="error_label">This field is required</span>}
						<input required id="name" type="text" {...register('name', { required: true })} placeholder="product's name"/>
					</div>
					<div className="form-control">
						<label htmlFor="price">Price</label>
						{errors.price && <span className="error_label">{errors.price.message}</span>}
						<input required id="price" type="number" {...register('price', { 
							required: {
								value: true,
								message: 'This field is required',
							}, 
							pattern: {
								value: /^[0-9]+$/,
								message: 'Please enter a number',
							}
						})} placeholder="please write only numbers here"/>
					</div>
					<div className="form-control switcher">
						<label htmlFor="type">Type switcher</label>
						{errors.type && <span className="error_label">{errors.type.message}</span>}
						<select {...register('type', { 
							required: {
								value: true,
								message: 'Please select a product type',
							}
						})} id="productType">
							<option value="">select a type</option>
							<option value="Dvd">DVD</option>
							<option value="Book">Book</option>
							<option value="Furniture">Furniture</option>
						</select>
					</div>
				</div>
				<div className="right">
					{watchProductType == "Dvd" && 
						<div id="DVD">
							<div className="form-control">
								<label htmlFor="size">Size (MB)</label>
								{errors.size && <span className="error_label">{errors.size.message}</span>}
								<input id="size" type="number" {...register('size', { 
									required: {
										value: true,
										message: 'This field is required',
									},
									pattern: {
										value: /^[0-9]+$/,
										message: 'Please enter a number',
									}
								})} placeholder="disc size in MB"/>
							</div>
							<p>This is a DVD disc product type, Please, provide it's size</p>
						</div>
					}

					{watchProductType == "Book" && 
						<div id="Book">
							<div className="form-control">
								<label htmlFor="weight">Weight (KG)</label>
								{errors.weight && <span className="error_label">{errors.weight.message}</span>}
								<input required id="weight" type="number" {...register('weight', { 
									required: {
										value: true,
										message: 'This field is required',
									},
									pattern: {
										value: /^[0-9]+$/,
										message: 'Please enter a number',
									}
								})} placeholder="book's weight in kg"/>
							</div>
							<p>This is a Book product type, Please, provide it's weight</p>
						</div>
					}

					{watchProductType == "Furniture" && 
						<div id="Furniture">
							<div className="form-control">
								<label htmlFor="height">Height (CM)</label>
								{errors.height && <span className="error_label">{errors.height.message}</span>}
								<input required id="height" type="number" {...register('height', { 
									required: {
										value: true,
										message: 'This field is required',
									},
									pattern: {
										value: /^[0-9]+$/,
										message: 'Please enter a number',
									}
								})} placeholder="product's height in cm"/>
							</div>
							<div className="form-control">
								<label htmlFor="width">Width (CM)</label>
								{errors.width && <span className="error_label">{errors.width.message}</span>}
								<input required id="width" type="number" {...register('width', { 
									required: {
										value: true,
										message: 'This field is required',
									},
									pattern: {
										value: /^[0-9]+$/,
										message: 'Please enter a number',
									}
								})} placeholder="product's width in cm"/>
							</div>
							<div className="form-control">
								<label htmlFor="lenght">Lenght (CM)</label>
								{errors.lenght && <span className="error_label">{errors.lenght.message}</span>}
								<input required id="length" type="number" {...register('lenght', { 
									required: {
										value: true,
										message: 'This field is required',
									},
									pattern: {
										value: /^[0-9]+$/,
										message: 'Please enter a number',
									}
								})} placeholder="product's lenght in cm"/>
							</div>
							<p>This is a Furniture product type, Please, provide it's dimensions</p>
						</div>
					}
				</div>
			</form>
		</div></>
    );
}